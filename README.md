CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module attempts to provide a user-friendly interface to browse referenced
entities from other entities.

It is a light version of [Entity Usage](https://www.drupal.org/project/entity_usage).


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


CONFIGURATION
-------------

 * Activate and configure this module per bundle of any entity type, for instance:
   Administration » Structure » Content Types » Page » Edit » Entity Usage (light) tab


MAINTAINERS
-----------

 * Current Maintainer: Matthieu Scarset - https://www.drupal.org/u/matthieuscarset


CREDITS
-----------

<a href="https://www.flaticon.com/free-icons/tree-structure" title="tree structure">Tree structure icon created by Freepik - Flaticon</a>
