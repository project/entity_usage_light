<?php

namespace Drupal\entity_usage_light\Form;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for entity_usage_light settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The plugin cache clearer.
   *
   * @var \Drupal\Core\Plugin\CachedDiscoveryClearerInterface
   */
  protected $pluginCacheClearer;

  /**
   * The route builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_usage_light_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_usage_light.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->pluginCacheClearer = $container->get('plugin.cache_clearer');
    $instance->routeBuilder = $container->get('router.builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_usage_light.settings');

    $entity_types = $this->entityTypeManager->getDefinitions();
    $entity_types = array_filter($entity_types, function (EntityTypeInterface $entity_type) {
      return $entity_type instanceof ContentEntityTypeInterface;
    });

    // Active on.
    $options = [];
    foreach ($entity_types as $entity_type_id => $entity_type) {
      $options[$entity_type_id] = $entity_type->getLabel();
    }

    $active_on_default_value = array_keys(array_filter($config->get('active_on') ?? []));

    $form['active_on'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content entity types'),
      '#options' => $options,
      '#default_value' => $active_on_default_value,
      '#description' => '<p>' . $this->t('Activate "Entity Usage (light)" tab on specific entity types.') . '</p>' .
      '<p>' . $this->t('Selected entity types will have an "Entity Usage (light)" tab in their local tasks.') . '</p>' .
      '<p>' . $this->t('Currently selected:&nbsp;<strong>@list</strong>', [
        '@list' => implode(', ', array_filter($active_on_default_value)),
      ]) . '</p>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('entity_usage_light.settings');
    $exclude = ['submit', 'form_build_id', 'form_token', 'form_id', 'op'];
    foreach ($form_state->getValues() as $key => $data) {
      if (in_array($key, $exclude)) {
        continue;
      }

      if ($key == 'active_on') {
        foreach ($data as $entity_type_id => $status) {
          $config->set("$key.$entity_type_id", (bool) $status);
        }
        continue;
      }

      // Save config values as-is by default.
      $config->set($key, $data);
    }

    $config->save();

    // Clear enough caches to enable/disable "Usage" tab for selected
    // entity types.
    $this->pluginCacheClearer->clearCachedDefinitions();
    $this->routeBuilder->rebuild();
    $this->messenger()->addStatus($this->t('Caches cleared.'));

    parent::submitForm($form, $form_state);
  }

}
