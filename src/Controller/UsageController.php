<?php

namespace Drupal\entity_usage_light\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for entity_usage_light module routes.
 */
class UsageController extends ControllerBase {

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Full list of entity IDs keyed by entity type after processing given entity.
   *
   * @var array
   */
  protected $referencedIds = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->fileUrlGenerator = $container->get('file_url_generator');
    $instance->cache = $container->get('cache.default');
    return $instance;
  }

  /**
   * Display the list of media for a given entity.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   A RouteMatch object.
   *
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function list(RouteMatchInterface $route_match) {
    $current_entity = $this->getEntityFromRouteMatch($route_match);
    if (!$current_entity instanceof EntityInterface) {
      return [
        '#markup' => $this->t('Missing entity reference.'),
      ];
    }

    $bundle_type = $current_entity->getEntityType()->getBundleEntityType();
    $bundle = $this->entityTypeManager()->getStorage($bundle_type)->load($current_entity->bundle());
    $settings = $bundle->getThirdPartySettings('entity_usage_light') ?? [];
    $selected_entity_types = $settings['entity_type_ids'] ?? [];
    $selected_views = $settings['entity_type_views'] ?? [];

    $build = [
      '#title' => $this->t('Referenced entities from @label', [
        '@label' => $current_entity->label(),
      ]),
    ];

    $build['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => FALSE,
    ];

    $build['settings']['summary'] = [
      '#theme' => 'item_list',
      '#title' => $this->t('Detectable entity types'),
      '#items' => array_map(function ($id) {
        return ['#markup' => $id];
      }, $selected_entity_types),
    ];

    $build['settings']['links'] = ['#type' => 'actions'];

    $build['settings']['links'][] = [
      '#type' => 'link',
      '#title' => $this->t('Configure entity usage light'),
      '#url' => $bundle->toUrl('edit-form', [
        'query' => ['destination' => Url::fromRoute('<current>')->toString()],
        'fragment' => 'edit-entity-usage-light',
      ]),
      '#attributes' => ['class' => ['button', 'button--primary']],
    ];

    if (empty($selected_entity_types)) {
      return $build;
    }

    // Get all referenced entities from cache.
    $cid = 'entity_usage_light:' . $current_entity->getEntityTypeId() . ':' . $current_entity->uuid();
    if ($cache = $this->cache->get($cid)) {
      $this->referencedIds = $cache->data;
    }

    // If no cache, calculate all referenced entities.
    if (!$cache || empty($this->referencedIds)) {
      foreach ($selected_entity_types as $entity_type_id) {
        $this->extractEntityIdsFromEntity($entity_type_id, $current_entity);
      }

      // Save results in cache.
      if (!$current_entity->isNew()) {
        $this->cache->set($cid, $this->referencedIds, Cache::PERMANENT, [
          $current_entity->getEntityTypeId() . ':' . $current_entity->id(),
        ]);
      }
    }

    foreach (array_filter($this->referencedIds) as $entity_type_id => $ids) {
      // Use custom Views if possible.
      if ($this->moduleHandler()->moduleExists('views')) {
        if ($view_id = $selected_views[$entity_type_id . '_usage_light_view'] ?? NULL) {
          if ($view = Views::getView($view_id)) {
            $view->setDisplay();
            $view->preExecute([implode('+', $ids)]);
            $view->execute();
            $build[$entity_type_id] = $view->render();
            continue;
          }
        }
      }

      // Use basic table by default.
      $build[$entity_type_id] = $this->getTable($entity_type_id, $ids);
    }

    return !empty($build) ? $build : [
      '#markup' => $this->t('No referenced entities.'),
    ];
  }

  /**
   * Retrieves entity from route match.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity object as determined from the passed-in route match.
   */
  protected function getEntityFromRouteMatch(RouteMatchInterface $route_match) {
    $parameter_name = $route_match->getRouteObject()->getOption('_usage_entity_type_id');
    return $route_match->getParameter($parameter_name);
  }

  /**
   * Get media IDs out of a given entity's fields.
   *
   * @param string $entity_type_id
   *   A given entity type ID.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   A given entity.
   *
   * @return array
   *   The list of entity IDs found in fields.
   */
  protected function extractEntityIdsFromEntity(string $entity_type_id, EntityInterface $entity) {
    // Get referenced entities (default).
    $ids = array_map(function ($entity) {
      return $entity->id();
    }, array_filter($entity->referencedEntities(), function ($ref) use ($entity_type_id) {
      return $ref->getEntityTypeId() == $entity_type_id;
    }));

    // Get entities fro specific fields.
    $fields = method_exists($entity, 'getFieldDefinitions') ? $entity->getFieldDefinitions() : [];

    // Recursive call for specific entity_reference.
    $entity_reference_fields = ['entity_reference', 'entity_reference_revisions', 'layout_section'];
    $entity_fields = array_filter($fields, function ($field_definition) use ($entity_reference_fields) {
      return in_array($field_definition->getType(), $entity_reference_fields);
    });
    foreach (array_keys($entity_fields) as $field_name) {
      // Skip. Field disappeared.
      if (!$entity->hasField($field_name)) {
        continue;
      }

      $items = $entity->get($field_name);
      switch ($items->getFieldDefinition()->getType()) {
        case 'layout_section':
          // Get inline block references from Layout Builder.
          $referenced_entities = [];
          foreach ($items->getSections() as $section) {
            foreach ($section->getComponents() as $component) {
              $plugin = $component->getPlugin();
              switch ($plugin->getBaseId()) {
                case 'inline_block':
                  $revision_id = $plugin->getConfiguration()['block_revision_id'];
                  $this->referencedIds['block_content'] += [$revision_id];
                  break;

                default:
                  // @todo What should we do if plugin unknown?
                  break;
              }
            }
          }
          break;

        default:
          // Safely get referenced entities.
          $referenced_entities = method_exists($items, 'referencedEntities') ? $items->referencedEntities() : [];
          break;
      }

      foreach ($referenced_entities as $referenced_entity) {
        $ids = array_merge($ids, $this->extractEntityIdsFromEntity($entity_type_id, $referenced_entity));
      }
    }

    // Extract embed entities.
    $text_fields = array_filter($fields, function ($field_definition) {
      return in_array($field_definition->getType(), ['text', 'text_long', 'text_with_summary']);
    }, ARRAY_FILTER_USE_BOTH);
    foreach (array_keys($text_fields) as $field_name) {
      $ids = array_merge($ids, $this->extractEntityIdsFromText($entity_type_id, $entity->get($field_name)->getString()));
    }

    // Clean values but keep anonymous user reference (e.g. $value == '0')
    $ids = array_unique(array_filter($ids, function ($value) {
      return $value !== NULL && $value !== '';
    }));

    if (!isset($this->referencedIds[$entity_type_id])) {
      $this->referencedIds[$entity_type_id] = [];
    }

    $this->referencedIds[$entity_type_id] = array_merge($ids, $this->referencedIds[$entity_type_id]);

    return $ids;
  }

  /**
   * Parse textfields to get <data-entity-XXX> attributes.
   *
   * @param string $entity_type_id
   *   A given entity type to look for.
   * @param string $text
   *   A given string, with HTML por favor.
   *
   * @return array
   *   The list of entity IDs found.
   *
   * @see \Drupal\media\Plugin\Filter\MediaEmbed::process();
   */
  protected function extractEntityIdsFromText(string $entity_type_id, string $text) {
    $ids   = [];
    $dom   = Html::load($text);
    $xpath = new \DOMXPath($dom);
    $query = $this->entityTypeManager()->getStorage($entity_type_id)->getQuery();

    if ($results = $xpath->query('//*[@data-entity-type="' . $entity_type_id . '" and normalize-space(@data-entity-uuid)!=""]')) {
      foreach ($results as $node) {
        $q    = clone $query;
        $uuid = $node->getAttribute('data-entity-uuid');
        $ids  = array_merge($ids, $q->condition('uuid', $uuid)->accessCheck(FALSE)->execute());
      }
    }

    return $ids;
  }

  /**
   * Prepare table render array to display entities.
   *
   * @param string $entity_type_id
   *   The entity type.
   * @param array $ids
   *   Given entity IDs.
   *
   * @return array
   *   The render array, obviously.
   */
  protected function getTable(string $entity_type_id, array $ids) {
    $manager      = $this->entityTypeManager();
    $entity_type  = $manager->getDefinition($entity_type_id);
    $storage      = $manager->getStorage($entity_type_id);
    $list_builder = $entity_type->hasListBuilderClass() ? $manager->getListBuilder($entity_type_id) : NULL;

    $rows = [];
    foreach ($storage->loadMultiple($ids) as $entity) {
      if ($entity_type_id == 'file') {
        $view_link = [
          '#type'  => 'link',
          '#url'   => Url::fromUri(
            $this->fileUrlGenerator->generateAbsoluteString($entity->getFileUri()),
            ['attributes' => ['target' => '_blank']]
          ),
          '#title' => $entity->label(),
        ];
        $actions = [];
      }
      else {
        $view_link = [
          '#type'               => 'link',
          '#url'                => $entity->hasLinkTemplate('canonical') ? $entity->toUrl('canonical') : Url::fromRoute('<nolink>'),
          '#title'              => $entity->label(),
          '#wrapper_attributes' => ['class' => ['label']],
        ];
        $edit_link = [
          '#type'               => 'link',
          '#url'                => $entity->hasLinkTemplate('edit-form') ? $entity->toUrl('edit-form') : Url::fromRoute('<nolink>'),
          '#title'              => $entity->label(),
          '#wrapper_attributes' => ['class' => ['label']],
        ];
        $actions = [
          '#type'  => 'link',
          '#url'   => $edit_link,
          '#title' => $this->t('Edit'),
        ];
        if ($list_builder) {
          $actions = [
            '#type'  => 'operations',
            '#links' => $list_builder->getOperations($entity),
          ];
        }
      }

      $row = [
        ['data' => $view_link],
        ['data' => $actions],
      ];
      $rows[] = $row;
    }

    $header = [
      $storage->getEntityType()->getPluralLabel(),
      ($entity_type_id == 'file') ? '' : $this->t('Links'),
    ];

    return [
      '#type'       => 'table',
      '#header'     => $header,
      '#rows'       => $rows,
      '#empty'      => $this->t('No @label found.', [
        '@label' => $entity_type->getPluralLabel(),
      ]),
      '#attributes' => [
        'class' => ['table-entity-usage', 'table-entity-usage--' . $entity_type_id],
      ],
      '#attached'   => [
        'library' => ['entity_usage_light/tables'],
      ],
    ];
  }

}
