<?php

namespace Drupal\entity_usage_light;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manipulates entity type information.
 */
class EntityTypeInfo implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * A config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * EntityTypeInfo constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\Config $config
   *   A config factory instance.
   */
  public function __construct(
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    Config $config
  ) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('config.factory')->get('entity_usage_light.settings')
    );
  }

  /**
   * Adds entity_usage_light links to appropriate entity types.
   *
   * This is an alter hook bridge.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   The master entity type list to alter.
   *
   * @see hook_entity_type_alter()
   */
  public function entityTypeAlter(array &$entity_types) {
    $entity_type_with_link_template = $this->config->get('active_on') ?? [];
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if (!in_array($entity_type_id, array_keys(array_filter($entity_type_with_link_template)))) {
        continue;
      }
      if (($entity_type->getFormClass('default') || $entity_type->getFormClass('edit')) && $entity_type->hasLinkTemplate('edit-form')) {
        $entity_type->setLinkTemplate('entity_usage_light', "/$entity_type_id/{{$entity_type_id}}/usage-light");
      }
    }
  }

  /**
   * Adds Usage Light operations on entity that supports it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity on which to define an operation.
   *
   * @return array
   *   An array of operation definitions.
   *
   * @see hook_entity_operation()
   */
  public function entityOperation(EntityInterface $entity) {
    $operations = [];
    if ($this->currentUser->hasPermission('access entity_usage_light information')) {
      if ($entity->hasLinkTemplate('usage_light')) {
        $operations['entity_usage_light'] = [
          'title'  => $this->t('Usage'),
          'weight' => 100,
          'url'    => $entity->toUrl('usage_light'),
        ];
      }
    }
    return $operations;
  }

  /**
   * Check if the form is a Bundle configuration form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityTypeInterface|null
   *   The bundle entity type or nothing.
   */
  public function getBundleTypeFromForm(FormStateInterface $form_state) {
    $form_object = $form_state->getFormObject();
    $bundle = method_exists($form_object, 'getEntity') ? $form_object->getEntity() : NULL;
    $bundle_type = $bundle ? $bundle->getEntityType() : NULL;
    return $bundle_type instanceof ConfigEntityTypeInterface ? $bundle_type : NULL;
  }

  /**
   * Custom form alter.
   *
   * @param array $form
   *   The form array, passed by reference.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function formAlter(array &$form, FormStateInterface $form_state) {
    // Display on bundle config edit form only.
    $bundle_type = $this->getBundleTypeFromForm($form_state);
    $bundle_of = $bundle_type ? $bundle_type->getBundleOf() : NULL;
    if (!$bundle_of) {
      return;
    }

    // Form element wrapper.
    $form['entity_usage_light'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Entity Usage (light)'),
      '#group' => 'additional_settings',
      '#tree' => TRUE,
    ];

    $active_on = $this->config->get('active_on') ?? [];
    if (!($active_on[$bundle_of] ?? FALSE)) {
      $form['entity_usage_light']['message'] = [
        '#type' => 'link',
        '#title' => $this->t('Enable entity usage on this entity type: @label', [
          '@label' => $bundle_of,
        ]),
        '#url' => Url::fromRoute('entity_usage_light.settings', [], [
          'query' => ['destination' => Url::fromRoute('<current>')->toString()],
        ]),
        '#attributes' => ['class' => ['button', 'button--primary']],
      ];
      return;
    }

    // Default values.
    $bundle = $form_state->getFormObject()->getEntity();
    $settings = $bundle->getThirdPartySettings('entity_usage_light');
    $entity_type_ids = $settings['entity_type_ids'] ?? [];
    $entity_type_views = $settings['entity_type_views'] ?? [];

    // Detectable entity types.
    $content_entity_types = array_filter($this->entityTypeManager->getDefinitions(), function (EntityTypeInterface $entity_type) {
      return $entity_type instanceof ContentEntityTypeInterface;
    });
    $entity_types_options = [];
    foreach ($content_entity_types as $entity_type_id => $entity_type) {
      $label = ucfirst($entity_type->getSingularLabel());
      $machine_name = str_replace([' ', '-'], '_', strtolower($entity_type->getSingularLabel()));
      if ($entity_type_id !== $machine_name) {
        $label .= '&nbsp;(' . $entity_type_id . ')';
      }
      $entity_types_options[$entity_type_id] = $label;
    }
    $form['entity_usage_light']['entity_type_ids'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Detectable entity types'),
      '#options' => $entity_types_options,
      '#default_value' => $entity_type_ids,
      '#description' => '<p>' . $this->t('Select which content should be displayed in "Usage" tab.') . '</p>',
    ];

    // Views.
    $form['entity_usage_light']['entity_type_views'] = [
      '#type' => 'details',
      '#title' => $this->t('Display'),
      '#description' => '<p>' . $this->t('Select the display in the usage tab per entity type.') . '</p><p>' . $this->t('A default table will be used if there is no view available for a given entity type.') . '</p>',
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $all_views = $this->entityTypeManager->getStorage('view')->loadMultiple();
    foreach ($content_entity_types as $entity_type_id => $entity_type) {
      $entity_type_views_options = [];
      foreach ($all_views as $view_id => $view) {
        if ($view_executable = $view->getExecutable()) {
          if ($view_base_entity_type = $view_executable->getBaseEntityType()) {
            if ($view_base_entity_type->id() == $entity_type_id) {
              $entity_type_views_options[$view_id] = $view->label();
            }
          }
        }
      }

      $form['entity_usage_light']['entity_type_views'][$entity_type_id . '_usage_light_view'] = [
        '#type'          => 'select',
        '#empty_option'  => $this->t('Default table'),
        '#options'       => $entity_type_views_options,
        '#title'         => $entity_type->getSingularLabel(),
        '#default_value' => $entity_type_views[$entity_type_id . '_usage_light_view'] ?? NULL,
        '#states'        => [
          'visible' => [
            'input[name="entity_usage_light[entity_type_ids][' . $entity_type_id . ']"]' => ['checked' => TRUE],
          ],
        ],
        '#access' => !empty($entity_type_views_options),
      ];
    }

    // Submit.
    $form['actions']['submit']['#validate'][] = [$this, 'setThirdPartySettings'];
  }

  /**
   * Save settings.
   */
  public function setThirdPartySettings(array $form, FormStateInterface $form_state) {
    // Save on bundle config edit form only.
    $bundle_type = $this->getBundleTypeFromForm($form_state);
    $bundle_of = $bundle_type ? $bundle_type->getBundleOf() : NULL;
    if (!$bundle_of) {
      return;
    }

    $bundle = $form_state->getFormObject()->getEntity();
    $settings = $form_state->getValue('entity_usage_light');
    $entity_type_ids = array_keys(array_filter($settings['entity_type_ids'] ?? []));
    $entity_type_views = array_filter($settings['entity_type_views'] ?? []);
    $bundle->setThirdPartySetting('entity_usage_light', 'entity_type_ids', $entity_type_ids);
    $bundle->setThirdPartySetting('entity_usage_light', 'entity_type_views', $entity_type_views);
  }

}
